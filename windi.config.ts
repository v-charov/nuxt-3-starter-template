import { defineConfig } from 'windicss/helpers'
import type { Plugin } from 'windicss/types/interfaces'

// - plugins
import FiltersPlugin from 'windicss/plugin/filters'
import TypographyPlugin from 'windicss/plugin/typography'
import AspectRatioPlugin from 'windicss/plugin/aspect-ratio'
import FormsPlugin from 'windicss/plugin/forms'

export default defineConfig({
  extract: {
    include: [
      './assets/scss/*.scss',
      './components/**/*.{vue,js}',
      './composables/**/*.{js,ts}',
      './content/**/*.md',
      './layouts/**/*.vue',
      './pages/**/*.vue',
      './plugins/**/*.{js,ts}',
      './utils/**/*.{js,ts}',
      './app.vue',
    ],
  },

  plugins: [
    // filters plugin require for navbar blur
    FiltersPlugin as Plugin,
    TypographyPlugin as Plugin,
    AspectRatioPlugin as Plugin,
    FormsPlugin as Plugin,
    require('@windicss/plugin-scrollbar'),
  ] as Plugin[],
})
