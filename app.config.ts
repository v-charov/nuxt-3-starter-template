export default defineAppConfig({
  name: 'Nuxt 3 Starter',
  author: {
    name: 'Vitaly Charov',
    link: 'https://gitlab.com/v-charov/',
  },
})
