module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:vue/vue3-recommended',
    'plugin:vue-pug/vue3-recommended',
    'plugin:@typescript-eslint/recommended',
    '@nuxtjs/eslint-config-typescript',
    'plugin:prettier-vue/recommended',
    'plugin:security/recommended',
  ],
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
    ecmaVersion: 2021,
  },
  rules: {
    // - prettier
    'prettier-vue/prettier': [
      'error',
      {
        singleQuote: true,
        semi: false,
        eol: 'lf',
        printWidth: 80,
        pugPrintWidth: 80,

        // - Prettier: pug
        pugFramework: 'vue',
        pugSingleQuote: false,
        pugAttributeSeparator: 'none',
        pugWrapAttributesThreshold: 1,
        pugEmptyAttributesForceQuotes: ['class', 'type', 'style', 'v-model'],
        pugClassNotation: 'attribute',
        pugEmptyAttributes: 'none',
      },
    ],

    // - pug
    'vue-pug/no-pug-control-flow': 'error',

    // - eslint
    'no-var': 'error',
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'comma-dangle': ['error', 'only-multiline'],
    '@typescript-eslint/no-unused-vars': ['error', { argsIgnorePattern: '_' }],
    'vue/block-lang': [
      'error',
      {
        script: { lang: 'ts' },
        template: { lang: 'pug' },
        style: { lang: 'scss' },
      },
    ],
    'vue/component-api-style': ['error', ['script-setup']],
    'vue/html-button-has-type': [
      'error',
      {
        button: true,
        submit: true,
        reset: true,
      },
    ],
    'vue/component-name-in-template-casing': [
      'error',
      'PascalCase',
      {
        registeredComponentsOnly: false,
      },
    ],
    'vue/custom-event-name-casing': 'error',
    'vue/define-macros-order': 'error',
    'vue/no-empty-component-block': 'error',
    'vue/no-multiple-objects-in-class': 'error',
    'vue/no-restricted-call-after-await': 'error',
    'vue/no-static-inline-styles': 'error',
    'vue/no-template-target-blank': 'error',
    'vue/no-undef-properties': 'error',
    'vue/no-useless-mustaches': 'error',
    'vue/no-useless-v-bind': 'error',
    'vue/padding-line-between-blocks': 'error',
    'vue/prefer-true-attribute-shorthand': 'error',
    'vue/require-emit-validator': 'error',
    'vue/v-for-delimiter-style': 'error',
    'vue/v-on-function-call': 'error',
    'vue/component-tags-order': [
      'error',
      { order: ['script[setup]', 'template', 'style'] },
    ],
    'vue/multi-word-component-names': 'off',
  },
  globals: {
    defineProps: 'readonly',
    defineEmits: 'readonly',
    defineExpose: 'readonly',
    withDefaults: 'readonly',
  },
}
