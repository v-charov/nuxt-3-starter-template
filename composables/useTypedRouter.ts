import { useTypedRouter } from '~/generated-router'

export default function () {
  return useTypedRouter()
}
