// - https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  // - css
  css: [
    'virtual:windi-base.css',
    'virtual:windi-components.css',
    'virtual:windi-utilities.css',
    'virtual:windi-devtools',
    'assets/scss/app.scss',
  ],

  imports: {
    dirs: ['stores'],
  },

  // - modules
  modules: [
    '@nuxt/content', // - https://content.nuxtjs.org/

    '@nuxtjs/html-validator', // - https://nuxt.com/modules/html-validator

    '@nuxt/image-edge', // - https://v1.image.nuxtjs.org/

    'nuxt-icons', // - https://nuxt.com/modules/icons

    'nuxt-windicss', // - https://pinia.vuejs.org/introduction.html

    [
      '@pinia/nuxt',
      {
        autoImports: [
          // - automatically imports `defineStore`
          'defineStore', // import { defineStore } from 'pinia'
          // - automatically imports `defineStore` as `definePiniaStore`
          ['defineStore', 'definePiniaStore'], // - import { defineStore as definePiniaStore } from 'pinia'
        ],
      },
    ], // - https://pinia.vuejs.org/

    'nuxt-typed-router', // - https://nuxt.com/modules/typed-router

    '@vueuse/nuxt', // -https://nuxt.com/modules/vueuse

    'nuxt-headlessui', // - https://headlessui.com/
  ],

  // - experimental
  experimental: {
    reactivityTransform: false,
    inlineSSRStyles: false,
  },

  // - @nuxtjs/html-validator
  htmlValidator: {
    usePrettier: false,
    logLevel: 'verbose',
    failOnError: false,
    options: {
      extends: [
        'html-validate:document',
        'html-validate:recommended',
        'html-validate:standard',
      ],
      rules: {
        'svg-focusable': 'off',
        'no-unknown-elements': 'error',
        // Conflicts or not needed as we use prettier formatting
        'void-style': 'off',
        'no-trailing-whitespace': 'off',
        // Conflict with Nuxt defaults
        'require-sri': 'off',
        'attribute-boolean-style': 'off',
        'doctype-style': 'off',
        // Unreasonable rule
        'no-inline-style': 'off',
      },
    },
  },

  // - @nuxt/image
  image: {
    dir: 'assets/images',
  },

  // - nuxt-windicss
  windicss: {
    analyze: {
      analysis: {
        interpretUtilities: false,
      },
      server: {
        port: 3333,
        open: false,
      },
    },
    scan: true,
  },

  // - nuxt-typed-router
  nuxtTypedRouter: {
    outDir: './generated-router',
  },

  // - @vueuse/nuxt
  vueuse: {
    ssrHandlers: true,
  },

  // - content
  content: {
    documentDriven: true,
    markdown: {
      mdc: true,
    },
    highlight: {
      theme: 'github-dark',
    },
  },

  // - https://nuxt.com/modules/headlessui
  headlessui: {
    prefix: 'Headless',
  },
})
